# soal-shift-sisop-modul-3-B09-2022


## Anggota Kelompok
| **Nama** | **NRP** |
| ------ | ------ |
| Helsa Nesta Dhaifullah | 5025201005 |
| Zunia Aswaroh | 5025201058 |
| Tengku Fredly Reinaldo | 5025201198 |

# Soal 1
Dikerjakan menggunakan:
- Thread & jointhread
- execv & wait
- read and write file pada bahasa C
- Fungsi yang digunakan -> wget, unzip & zip dengan password, rm, mkdir, move file

### Poin A
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
1. Main Function

```
int main(void)
{
	// char *argv8[] = {"rm", "quote.zip", NULL};
	// char *argv9[] = {"rm", "music.zip", NULL};
	child2 = fork();
	int status2 = 0;
	int i=0;
	int err;
	if(child2==0){
		while(i<4) // loop sejumlah thread
		{
			err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
			if(err!=0) //cek error
			{
				printf("\n can't create thread : [%s]",strerror(err));
			}
			else
			{
				printf("\n create thread success\n");
			}
			i++;
		}
		pthread_join(tid[0],NULL);
		pthread_join(tid[1],NULL);
		pthread_join(tid[2],NULL);
		pthread_join(tid[3],NULL);
	int main(void)
{
	// char *argv8[] = {"rm", "quote.zip", NULL};
	// char *argv9[] = {"rm", "music.zip", NULL};
	child2 = fork();
	int status2 = 0;
	int i=0;
	int err;
	if(child2==0){
		while(i<4) // loop sejumlah thread
		{
			err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
			if(err!=0) //cek error
			{
				printf("\n can't create thread : [%s]",strerror(err));
			}
			else
			{
				printf("\n create thread success\n");
			}
			i++;
		}
		pthread_join(tid[0],NULL);
		pthread_join(tid[1],NULL);
		pthread_join(tid[2],NULL);
		pthread_join(tid[3],NULL);
	
```
2. Makedir modul, hasil, download dengan wget, dan unzip music.zip & quote.zip
```
void* playandcount(void *arg)
{
	char *argv1[] = {"wget", "--no-check-certificate", "-q", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "music.zip", NULL};
	char *argv2[] = {"wget", "--no-check-certificate", "-q", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "quote.zip", NULL};
	char *argv3[] = {"unzip", "music.zip", "-d", "./modul/music", NULL};
    char *argv4[] = {"unzip", "quote.zip", "-d", "./modul/quote", NULL};
	char *argv7[] = {"mkdir", "./modul", NULL};
	char *argv8[] = {"mkdir", "./hasil", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid[0])) 
	{
		child = fork();
		int status=0;
		if (child==0) {
		    execv("/usr/bin/mkdir", argv7);
	    }
	}
	else if(pthread_equal(id,tid[1])){
		child = fork();
		int status=0;
		if (child==0) {
		    execv("/usr/bin/mkdir", argv8);
	    }
	}
	else if(pthread_equal(id,tid[2])) //thread untuk download music.zip
	{
		child = fork();
        int status = 0;
		if (child==0) {
		    execv("/usr/bin/wget", argv1);
		}
	}
	else if(pthread_equal(id,tid[3])) // thread untuk download quote.zip
	{
        child = fork();
        int status = 0;
        if (child==0) {
		    execv("/usr/bin/wget", argv2);
	    }
		else{
            while ((wait(&status)) > 0);
			child1 = fork();
			int status1 = 0;
			if(child1==0){
			    execv("/usr/bin/unzip", argv3); 
			}
			else{
				while ((wait(&status1)) > 0);
				execv("/usr/bin/unzip", argv4);
			}
		}
	}

	return NULL;
}
```
3. Delete file music.zip dan quote.zip
```
else{
		while ((wait(&status2)) > 0);
		deleteFile();
	}
```

> deleteFile Func
```
void deleteFile(){
	char *argv8[] = {"rm", "quote.zip", NULL};
	char *argv9[] = {"rm", "music.zip", NULL};
	child = fork();
	int status = 0;
	if(child==0){
		execv("/usr/bin/rm", argv8);
	}
	else{
		while ((wait(&status)) > 0);
		child1 = fork();
		int status1 = 0;
		if(child1==0){
			execv("/usr/bin/rm", argv9);
		}
		else{
			while ((wait(&status1)) > 0);
			playthread();
		}
	}
}
```
### Poin B
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

1. Memanggil fungsi playthread()
```
else{
    while ((wait(&status1)) > 0);
    playthread();
}
```
2. playthread func
```
void playthread(){
	child=fork();
	int status=0;
	if(child==0){
		int err;
		unsigned long i =0;
		while(i<2) // loop sejumlah thread
		{
			err=pthread_create(&(tid1[i]),NULL,&base64,NULL); //membuat thread
			if(err!=0) //cek error
			{
				printf("\n can't create thread : [%s]",strerror(err));
			}
			else
			{
				printf("\n create thread success\n");
			}
			i++;
		}
		pthread_join(tid1[0],NULL);
		pthread_join(tid1[1],NULL);
	}else{
		while(wait(&status)>0);
		sleep(5);
		// printf("succes\n");
		moveMusic("music.txt", "./hasil");
	}
}
```
3. Base64 func()
```
void* base64(void *argv)
{	
	child=fork();
	int status = 0;
	pthread_t id=pthread_self();
	if(child==0)
	{
		if(pthread_equal(id,tid1[0]))
		{
			child = fork();
			int status = 0;
			if(child==0)
			{
				char encoded_string[255];
				char decoded_string[255];
				char path[1000];
				char textPath[1000];
				struct dirent *dp;
				FILE *fp;
				FILE *fp1;
				strcpy(textPath, musicPath);
				strcat(textPath, "music.txt");
				fp1 = fopen(textPath, "w");

				DIR *dir = opendir(musicPath);

				if (!dir)
					return 0;

				while ((dp = readdir(dir)) != NULL)
				{
					if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "music.txt") !=0 )
					{
						strcpy(path, musicPath);
						strcat(path, "/");
						strcat(path, dp->d_name);
						
						fp = fopen (path, "r");
						fscanf(fp, "%s", encoded_string);
						int len_str = strlen(encoded_string);
						fprintf(fp1, "%s\n", base64Decoder(encoded_string, len_str));
					}
				}
				fclose(fp);
				closedir(dir);
			}	
		} 
		else if(pthread_equal(id,tid1[1]))
		{
			child = fork();
			int status = 0;
			if(child==0)
			{
				char encoded_string[255];
				char decoded_string[255];
				char path[1000];
				char textPath[1000];
				struct dirent *dp;
				FILE *fp;
				FILE *fp1;
				strcpy(textPath, quotePath);
				strcat(textPath, "quote.txt");
				fp1 = fopen(textPath, "w");

				DIR *dir = opendir(quotePath);

				if (!dir)
					return 0;

				while ((dp = readdir(dir)) != NULL)
				{
					if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "quote.txt") != 0)
					{
						strcpy(path, quotePath);
						strcat(path, "/");
						strcat(path, dp->d_name);
						
						fp = fopen (path, "r");
						fscanf(fp, "%s", encoded_string);
						int len_str = strlen(encoded_string);
						fprintf(fp1, "%s\n", base64Decoder(encoded_string, len_str));
					}
				}
				closedir(dir);
			}	
		}
	}
}
```

4. base64Decoder func (copy from gfg)
```
char* base64Decoder(char encoded[], int len_str)
{
    char* decoded_string;
    // FILE *fp1;
    // fp1 = fopen("/home/helsanesta/Documents/modul_3/modul/music/music.txt", "w");

    decoded_string = (char*)malloc(sizeof(char) * SIZE);
 
    int i, j, k = 0;
 
    // stores the bitstream.
    int num = 0;
 
    // count_bits stores current
    // number of bits in num.
    int count_bits = 0;
 
    // selects 4 characters from
    // encoded string at a time.
    // find the position of each encoded
    // character in char_set and stores in num.
    for (i = 0; i < len_str; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++) {
            // make space for 6 bits.
            if (encoded[i + j] != '=') {
                num = num << 6;
                count_bits += 6;
            }
 
            /* Finding the position of each encoded
            character in char_set
            and storing in "num", use OR
            '|' operator to store bits.*/
 
            // encoded[i + j] = 'E', 'E' - 'A' = 5
            // 'E' has 5th position in char_set.
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
 
            // encoded[i + j] = 'e', 'e' - 'a' = 5,
            // 5 + 26 = 31, 'e' has 31st position in char_set.
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
 
            // encoded[i + j] = '8', '8' - '0' = 8
            // 8 + 52 = 60, '8' has 60th position in char_set.
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
 
            // '+' occurs in 62nd position in char_set.
            else if (encoded[i + j] == '+')
                num = num | 62;
 
            // '/' occurs in 63rd position in char_set.
            else if (encoded[i + j] == '/')
                num = num | 63;
 
            // ( str[i + j] == '=' ) remove 2 bits
            // to delete appended bits during encoding.
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }
 
        while (count_bits != 0) {
            count_bits -= 8;
 
            // 255 in binary is 11111111
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }
 
    // place NULL character to mark end of string.
    decoded_string[k] = '\0';

    return decoded_string;
}
```

### Poin C
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

1. Move file music.txt
```
else{
    while(wait(&status)>0);
    sleep(5);
    moveMusic("music.txt", "./hasil");
}
```
2. moveMusic func()
```
void moveMusic(char source[30], char dest[30]){
    char pathfile[50];
	strcpy(pathfile, musicPath);
	strcat(pathfile, source);
	child = fork();
	int status=0;
	char *argv[] = {"mv", pathfile, dest,NULL};
	
	if(child==0){
		// printf("GOOOO\n");
		// execv("bin/mkdir", argv);
		execv("/bin/mv", argv);
	}else{
		while(wait(&status)>0);
		moveQuote("quote.txt", "./hasil");
	}
}
```
3. move file quote.txt
```
else{
    while(wait(&status)>0);
    moveQuote("quote.txt", "./hasil");
}
```
4. moveQuote func
```
void moveQuote(char source[30], char dest[30]){
    char pathfile[50];
	strcpy(pathfile, quotePath);
	strcat(pathfile, source);
	child = fork();
	int status=0;
	char *argv[] = {"mv", pathfile, dest,NULL};
	
	if(child==0){
		// printf("GOOOO\n");
		// execv("bin/mkdir", argv);
		execv("/bin/mv", argv);
	}
	else{
		while(wait(&status)>0);
		zipping();
	}
}
```

### Poin D
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

1. Call zipping func
```
else{
    while(wait(&status)>0);
    zipping();
}
```
2. Zipping Func
```
void zipping(){
  child = fork();
  int status=0;
  if(child == 0){
    char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenesthelsa", "hasil", NULL}; //mengcompile folder gacha_gacha ke dalam bentuk zip sekaligus memberikan passwordnya
    execv("/usr/bin/zip", args);
  }
  else {
	while(wait(&status)>0);
	printf("unziphasil succes");
	unziphasil();
  }
}
```

### Poin E
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

1. Call unziphasil() untuk mengunzip
```
void unziphasil(){
	child = fork();
	int status = 0;
	if(child==0){
		unsigned long i =0;
		int err;
		while(i<2) // loop sejumlah thread
		{
			err=pthread_create(&(tid2[i]),NULL,&unzippp,NULL); 
			if(err!=0) //cek error
			{
				printf("\n can't create thread : [%s]",strerror(err));
			}
			else
			{
				printf("\n create thread success\n");
			}
			i++;
		}
		pthread_join(tid2[0],NULL);
		pthread_join(tid2[1],NULL);
	}
	else {
		while ((wait(&status)) > 0);
		char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenesthelsa", "hasil", "no.txt", NULL}; //mengcompile folder gacha_gacha ke dalam bentuk zip sekaligus memberikan passwordnya
        execv("/usr/bin/zip", args);
  	}
}
```
2. Memanggil fungsi thread unzippp
```
void* unzippp(void *arg) {
	unsigned long i=0;
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid2[0])) //thread untuk clear layar
	{
		startunzip();
	}
	else if(pthread_equal(id,tid2[1])) // thread menampilkan counter
	{	
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL)
		{
			printf("Error opening the file %s", filename);
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	}
}
```
3. Memanggil fungsi startunzip()
```
void startunzip(){
  child = fork();
  int status = 0;
  if(child == 0){
    char *argv[] = {"unzip", "-P","mihinomenesthelsa", "hasil.zip",  NULL};
	execv ("/usr/bin/unzip",argv);
  }else {
	while(wait(&status)>0);
    printf("tutup\n");
  }
}
```
4. Melakukan zip ulang hasil dan file no.txt
```
else {
    while ((wait(&status)) > 0);
    char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenesthelsa", "hasil", "no.txt", NULL}; //mengcompile folder gacha_gacha ke dalam bentuk zip sekaligus memberikan passwordnya
    execv("/usr/bin/zip", args);
}
```

## Kendala Soal No 1
Agak kebingungan ketika membuat thread dan jointhread, sehingga untuk mempermudah saya membuat banyak fungsi untuk setiap tugas yg akan dijalankan. Kebanyakan masih menggunakan materi pada modul 2 tentang exec dan wait. Setiap fungsi yang saya tuliskan di atas dijalankan menggunakan execv.
